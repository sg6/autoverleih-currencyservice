﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.Collections;
using System.IO;



[WebService(Namespace = "http://sintratecalc.azurewebsites.net/", Description = "Exchange Rate Calculator", Name = "RateCalculatorWebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]

public class Service : System.Web.Services.WebService
{
    public Service () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    


    [WebMethod]
    public XmlDocument getCurrencyRate(String cur1, String cur2)
    {
        if (validateCurrency(cur1) && validateCurrency(cur2))
        {

            XmlReader xmlReader = XmlReader.Create("http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml");

            XmlDocument doc = new XmlDocument();

            XmlElement element1 = doc.CreateElement(string.Empty, "body", string.Empty);
            doc.AppendChild(element1);

            XmlElement element2 = doc.CreateElement(string.Empty, "ExchangeCurrency", string.Empty);
            element1.AppendChild(element2);

            XmlElement element3 = doc.CreateElement(string.Empty, "fromCurrency", string.Empty);
            XmlText text = doc.CreateTextNode(cur1);
            element3.AppendChild(text);
            element2.AppendChild(element3);

            element3 = doc.CreateElement(string.Empty, "toCurrency", string.Empty);
            text = doc.CreateTextNode(cur2);
            element3.AppendChild(text);
            element2.AppendChild(element3);

            Double currency1 = 1.0;
            Double currency2 = 1.0;

            while (xmlReader.Read())
            {
                if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "Cube"))
                {
                    if (xmlReader.HasAttributes && xmlReader.GetAttribute("currency") == cur1 && cur1 != "EUR")
                    {

                        currency1 = double.Parse(xmlReader.GetAttribute("rate"));
                    }

                    if (xmlReader.HasAttributes && xmlReader.GetAttribute("currency") == cur2)
                    {
                        currency2 = double.Parse(xmlReader.GetAttribute("rate"));
                    }

                }
            }

            Double rate = currency2 / currency1;
            element3 = doc.CreateElement(string.Empty, "rate", string.Empty);
            text = doc.CreateTextNode(rate.ToString());
            element3.AppendChild(text);
            element2.AppendChild(element3);
            return doc;
        }
        else
        {
            XmlDocument doc = new XmlDocument();
            XmlElement element1 = doc.CreateElement(string.Empty, "body", string.Empty);
            doc.AppendChild(element1);

            XmlElement element2 = doc.CreateElement(string.Empty, "ExchangeCurrency", string.Empty);
            element1.AppendChild(element2);

            XmlElement element3 = doc.CreateElement(string.Empty, "ERROR", string.Empty);
            XmlText text = doc.CreateTextNode("CURRENCY NOT VALID");
            element3.AppendChild(text);
            element2.AppendChild(element3);

            return doc;
        }
    }

    [WebMethod]
    public XmlDocument  getAllCurrencies()
    {
        XmlDocument doc = new XmlDocument();
        XmlElement root = doc.DocumentElement;

        XmlElement element1 = doc.CreateElement(string.Empty, "body", string.Empty);
        doc.AppendChild(element1);

        
        try { 
            XmlReader xmlReader = XmlReader.Create("http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml");

            XmlElement element2 = doc.CreateElement(string.Empty, "currencies", string.Empty);
            element1.AppendChild(element2);

            XmlElement element3 = null;

            element3 = doc.CreateElement(string.Empty, "currency", string.Empty);
            XmlText text = doc.CreateTextNode("EUR");
            element3.AppendChild(text);
            element2.AppendChild(element3);
            while (xmlReader.Read())
            {
                if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "Cube"))
                {
                    if (xmlReader.HasAttributes && xmlReader.GetAttribute("currency") != null)
                    {
                        element3 = doc.CreateElement(string.Empty, "currency", string.Empty);
                        text = doc.CreateTextNode(xmlReader.GetAttribute("currency"));
                        element3.AppendChild(text);
                        element2.AppendChild(element3);
                    }

                }
            }
        }
        catch (Exception)
        {
            XmlElement element2 = doc.CreateElement(string.Empty, "ERROR", string.Empty);
            XmlText text = doc.CreateTextNode("ERROR NO CONNECTION");
            element2.AppendChild(text);
            element1.AppendChild(element2);
            return doc;
        }

        return doc; 
    }
    public Boolean validateCurrency(String cur)
    {
        ArrayList retval = new ArrayList();

        XmlReader xmlReader = XmlReader.Create("http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml");

        if (cur == "EUR")
        {
            return true;
        }

        while (xmlReader.Read())
        {
            if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "Cube"))
            {
                if (xmlReader.HasAttributes && xmlReader.GetAttribute("currency") != null)
                {
                    if (xmlReader.GetAttribute("currency") == cur)
                    {
                        return true;
                    }
                }

            }
        }
        return false;
    }
    
}